This is a project to test frontend developers and designers

**Beware:** some links can be broken because some pages are removed

# How to launch

* `npm i` to install packages
* Install [Hugo](https://gohugo.io/getting-started/quick-start/#step-1-install-hugo) 
* `npm run watch` to start a development server
* open [http://localhost:8080](http://localhost:8080)

# Important to consider

There are several files to check the styles:

* `.stylelintrc` for CSS/LESS files
* `.eslintrc` for JS files
* `.editorconfig` for general editor settings

You must use them to deliver the code which is according to the style 
