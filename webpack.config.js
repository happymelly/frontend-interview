const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WebpackShellPlugin = require('webpack-shell-plugin');
const path = require('path');
const webpack = require('webpack');

const extractLess = new ExtractTextPlugin({
  filename: 'css/[name].css',
  disable: process.env.NODE_ENV === 'development'
});

const hugoSrc = path.resolve(__dirname, 'site');
const dest = path.resolve(__dirname, 'dist');

const hugoDev = `hugo --buildDrafts --watch --source ${hugoSrc} --destination ${dest}`;
const hugoProd = `hugo --source ${hugoSrc} --destination ${dest}`;

module.exports = (env) => {
  const isDev = env === 'dev';

  return {
    devtool: isDev ? 'inline-source-map' : false,
    context: path.resolve(__dirname, 'src'),
    devServer: {
      contentBase: dest,
      watchContentBase: true
    },
    entry: {
      app: './app/app.js',
      main: './styles/main.less'
    },
    externals: {
      'jquery': 'jQuery',
    },
    output: {
      filename: '[name].js',
      path: path.resolve(__dirname, 'site', 'static')
    },
    module: {
      rules: [
        {
          test: /\.((png)|(eot)|(woff)|(woff2)|(ttf)|(svg)|(gif))(\?v=\d+\.\d+\.\d+)?$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: path.resolve(__dirname, 'site', 'static', 'fonts', '[name].[ext]')
              }
            }
          ]
        },
        {
          test: /\.ts?$/,
          use: 'ts-loader',
          exclude: /node_modules/
        },
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['env']
            }
          }
        },
        {
          test: /\.less$/,
          exclude: /node_modules/,
          use: extractLess.extract({
            use: [
              {
                loader: 'css-loader',
                options: {sourceMap: isDev}
              },
              {
                loader: 'postcss-loader',
                options: {sourceMap: isDev}
              },
              {
                loader: 'less-loader',
                options: {sourceMap: isDev}
              }
            ],
            fallback: 'style-loader'
          })
        },
      ]
    },
    plugins: [
      new CleanWebpackPlugin([
        path.resolve(__dirname, 'dist'),
        path.resolve(__dirname, 'site', 'data'),
        path.resolve(__dirname, 'site', 'static', 'fonts'),
        path.resolve(__dirname, 'site', 'static', 'css'),
        path.resolve(__dirname, 'site', 'static', 'js')
      ]),
      extractLess,
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
      }),
      new WebpackShellPlugin({
        onBuildEnd: isDev ? hugoDev : hugoProd
      })
    ],
    resolve: {
      extensions: ['.js', '.ts']
    }
  };
};
