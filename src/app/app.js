import PubSub from './libs/_pubsub.js';
import SocialSharing from './widgets/_social-sharing.js';
import Subscription from './widgets/_subscription.js';
import Cookie from './widgets/_cookie.js';
import Tabs from './widgets/_tabs.js';
import Slider from './widgets/_slider';
import VideoPopup from './widgets/_video-popup.js';
import Navigation from './widgets/_navigation.js';
import TopAnouncement from './widgets/_top-anouncement.js';
import TabsAttendee from './widgets/_tabs-attendee.js';
import VideoPreview from './widgets/_video-preview.js';

window.App.events = PubSub;

$(function() {

  const $body = $('body');
  const $window = $(window);

  $body
    .on('click', '[data-menu-blog]', (e) => {
      e.preventDefault();
      $('.m-nav__con').slideToggle();
    })
    .on('click', '[data-menu-site]', (e) => {
      e.preventDefault();
      const isShow = !$body.hasClass('s-site_show_menu');

      if (!isShow) {
        $body.removeClass('s-site_show_menu');
        return;
      }

      const isSetHeightMenu = $window.height() < $('[data-menu-place]').outerHeight();
      $('[data-menu-con]').css('height', isSetHeightMenu ? $window.height() : 'auto');

      $body.addClass('s-site_show_menu');
      $('html, body').animate({
        scrollTop: 0
      }, 300);
    })
    .on('click', '[data-toggle-features]', (e) => {
      e.preventDefault();
      const $link = $(e.currentTarget);

      $link.closest('.s-pay').toggleClass('s-pay_state_more');
    })
    .on('click', '[data-toggle-faq]', (e) => {
      e.preventDefault();
      const $link = $(e.currentTarget);

      if ($(window).width() > 920) return false;

      $link.toggleClass('state_open');
    });

  const $nav = $('.b-nav');
  $window.scroll(function() {
    $nav && $nav.toggleClass('b-nav_show_fix', $window.scrollTop() > 430);
  });

  Cookie.plugin('.js-cookie-block');
  SocialSharing.plugin('.js-social-sharing');
  Subscription.plugin('.js-subs');
  Tabs.plugin('.js-tabs');
  VideoPopup.plugin('.js-video-popup');
  Navigation.plugin('.js-navigation');
  TopAnouncement.plugin('.top-anouncement');
  TabsAttendee.plugin('.js-tabs-attendee');
  Slider.plugin('.js-slider');
  VideoPreview.plugin('.js-video-preview');
});
