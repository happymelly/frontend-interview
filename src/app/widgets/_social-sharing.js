

export default class Widget {
  constructor(elem, params) {
    this.$root = $(elem);
    this.params = $.extend({}, params, this.$root.data());
    this.config = {
      facebook: {
        sharerUrl: 'https://www.facebook.com/sharer/sharer.php?'
      },
      twitter: {
        sharerUrl: 'http://twitter.com/intent/tweet?'
      },
      linkedin: {
        sharerUrl: 'https://www.linkedin.com/shareArticle?'
      },
      gplus: {
        sharerUrl: 'https://plus.google.com/share?'
      }
    };
    this.init();
  }

  init() {
    if (this.$root.length === 0) {
      console.log('No root for sharing widget');
      return;
    }

    if (!this.params.link) {
      this.params.link = location.protocol + '//' + location.hostname + location.pathname;
    }

    if (!this.params.title) {
      this.params.title = document.title;
    }

    this.urlBuilders = this._getSocialConfig();

    this._buildElements();
    this._assignEvents();
  }

  _buildElements() {
    const self = this;

    self.$shareLinks = self.$root.find('[data-share-link]').attr('target', '_blank');

    self.$shareLinks.each((index, element) => {
      const $el = $(element);
      $el.attr('href', self.urlBuilders[$el.attr('data-share-link')].bind(self)());
    });
  }

  _assignEvents() {
    this.$root
      .on('click', '[data-share-link]', this._onClickShare.bind(this))
      .on('click', '[data-share-live]', this._onClickShareIframe.bind(this));
  }

  _onClickShare(e) {
    e.preventDefault();
    const url = $(e.currentTarget).attr('href');

    window.open(url,
      'name',
      'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=500');
  }

  _onClickShareIframe(e) {
    e.preventDefault();
    const social = $(e.currentTarget).data('share-live');

    const urlBuilders = this._getSocialConfig();
    this.params.link = '';

    App.events.pub('hmt.widget.command', {
      method: 'openSharingDialog',
      social: social,
      url: urlBuilders[social]()
    });
  }

  _getSocialConfig() {
    const self = this;

    return {
      facebook: function() {
        var data = {
          u: self.params.facebookLink || self.params.link,
          picture: self.params.image,
          source: self.params.source,
          t: self.params.title,
          text: self.params.summary
        };

        for (var param in data) {
          if (data.hasOwnProperty(param) && !data[param]) {
            delete data[param];
          }
        }

        return self.config.facebook.sharerUrl + $.param(data);
      },
      twitter: function() {
        var data = {
          url: self.params.twitterLink || self.params.link,
          text: self.params.twitterText || self.params.summary,
          via: self.config.twitter.via || null,
          hashtags: self.params.hashtags || null,
          related: self.params.related || null
        };

        for (var param in data) {
          if (data.hasOwnProperty(param) && !data[param]) {
            delete data[param];
          }
        }

        return self.config.twitter.sharerUrl + $.param(data);
      },
      linkedin: function() {
        var data = {
          url: self.params.linkedInLink || self.params.link,
          source: self.params.title,
          summary: self.params.summary,
          image: self.params.image,
          mini: true
        };

        for (var param in data) {
          if (data.hasOwnProperty(param) && !data[param]) {
            delete data[param];
          }
        }

        return self.config.linkedin.sharerUrl + $.param(data);
      },
      gplus: function() {
        var data = {
          url: self.params.gplusLink || self.params.link
        };
        return self.config.gplus.sharerUrl + $.param(data);
      }
    };
  }

  static plugin(selector, options) {
    const $elems = $(selector);
    if (!$elems.length) return;

    return $elems.each(function(index, el) {
      const $element = $(el);
      let data = $element.data('widget.social.sharing');

      if (!data) {
        data = new Widget(el, options);
        $element.data('widget.social.sharing', data);
      }
    });
  }
}

