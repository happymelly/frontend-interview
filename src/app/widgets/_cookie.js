

export default class Widget {
  constructor(elem) {
    this.$root = $(elem);

    if (!this.readCookie('wb_cookie')) {
      this._showBanner();
    }
    this._assignEvents();
  }

  _assignEvents() {
    this.$root
      .on('click', '[data-cookie-btn]', this._onClickAccept.bind(this));
  }

  _onClickAccept(e) {
    e.preventDefault();
    this.$root.removeClass('state_show');
    this.createCookie('wb_cookie', 'accept', 360);
  }

  _showBanner() {
    this.$root.addClass('state_show');
  }

  createCookie(name, value, days) {
    let date, expires;

    if (days) {
      date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = '; expires=' + date.toGMTString();
    }
    else {
      expires = '';
    }
    document.cookie = name + '=' + value + expires + '; path=/';
  }

  readCookie(name) {
    const nameEQ = name + '=';
    const ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === ' ') c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
  }

  static plugin(selector) {
    const $elems = $(selector);
    if (!$elems.length) return;

    return $elems.each(function(index, el) {
      const $element = $(el);
      let data = $element.data('widget.cookie');

      if (!data) {
        data = new Widget(el);
        $element.data('widget.cookie', data);
      }
    });
  }
}

