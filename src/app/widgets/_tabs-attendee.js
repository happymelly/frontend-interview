

export default class Widget {
  constructor(elem) {
    this.$root = $(elem);
    this.locals = this._getDom();

    this.setActiveTab(0);
    this._assignEvents();
  }

  _getDom() {
    const $root = this.$root;

    return {
      $links: $root.find('[data-tabs-link]'),
      $tabContents: $root.find('[data-tabs-content]')
    };
  }

  _assignEvents() {
    this.$root
      .on('click', '[data-tabs-link]', this._onClickTab.bind(this));
  }

  _onClickTab(e) {
    e.preventDefault();
    const $link = $(e.currentTarget);

    if ($link.hasClass('state_active')) return;

    this.setActiveTab($link.index());
  }

  setActiveTab(index) {
    const locals = this.locals;
    const $activeLink = this.locals.$links.eq(index);
    const target = $activeLink.attr('href');

    locals.$links
      .removeClass('state_active')
      .filter(`[href="${target}"]`)
      .addClass('state_active');
    locals.$tabContents.removeClass('state_active');
    $(target).addClass('state_active');
  }

  static plugin(selector) {
    const $elems = $(selector);
    if (!$elems.length) return;

    return $elems.each(function(index, el) {
      const $element = $(el);
      let data = $element.data('widget.tabs.attendee');

      if (!data) {
        data = new Widget(el);
        $element.data('widget.tabs.attendee', data);
      }
    });
  }
}
