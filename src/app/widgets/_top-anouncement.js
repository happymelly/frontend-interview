// Ussage:
// Attribute: data-feature-unavailable="training-companies, brands"
// is added on a section that is unavailable for one or two customer types
// Example: <div data-feature-unavailable="training-companies, brands" class="integration-possibilities"></div>

export default class Widget {

  constructor(element) {
    this.$root = $(element);

    this._init();
  }

  _init() {
    var $unavailableFeatures = $( "[data-feature-unavailable]" );
    if( $unavailableFeatures.length ){
      var $topAnouncement = this.$root;

      // Sections
      $unavailableFeatures.each( function(index, feature) {
        // : string[]
        // Unavailable customers defined in section's attribute
        // For example: [data-feature-unavailable]="training-companies, trainers"
        var unavailableCustomers = $(feature).attr("data-feature-unavailable").split(",");

        unavailableCustomers.forEach( function(unavailableCustomer) {
          unavailableCustomer = unavailableCustomer.trim()
          $topAnouncement
            .find(`[data-customer-type="${unavailableCustomer}"]`)
            .addClass("unavailable-feature");
        });

      }
    );
  }
  }

  static plugin(selector) {
    const $el = $(selector);
    if (!$el.length) return;

    let data = $el.data('widget.top-anouncement');
    if( !data ) {
        data = new Widget($el);
        $el.data('widget.top-anouncement', data);
    }
  }

}
