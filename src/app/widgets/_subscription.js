

export default class Widget {
  constructor(elem) {
    this.$root = $(elem);
    this.locals = this._getDom();

    this._assignEvents();
  }

  _getDom() {
    const $root = this.$root;

    return {
      $form: $root.find('[data-subs-form]'),
      $input: $root.find('[data-subs-input]')
    };
  }

  _assignEvents() {
    this.$root.on('click', '[data-subs-btn]', this._onClickResub.bind(this));
    this.locals.$form.on('submit', this._onSubmitForm.bind(this));
  }

  _onClickResub(e) {
    e.preventDefault();
    this.$root.removeClass('b-subscribe_state_final');
  }

  _onSubmitForm(e) {
    e.preventDefault();
    const self = this;
    const $input = self.locals.$input;
    const value = $input.val();

    if (!value) {
      $input.val('');
      $input.trigger('focus');
      return;
    }

    self._sendData(value)
      .done(() => {
        $input.val('');
        self.$root.addClass('b-subscribe_state_final');
      });
  }

  _sendData(value) {
    const self = this;
    const locals = self.locals;
    const params = {
      url: locals.$form.attr('action'),
      method: locals.$form.attr('method'),
      data: locals.$form.serialize(),
      cache       : false,
      dataType    : 'json'
    };

    return $.ajax(params);
  }

  static plugin(selector) {
    const $elems = $(selector);
    if (!$elems.length) return;

    return $elems.each(function(index, el) {
      const $element = $(el);
      let data = $element.data('widget.subscription');

      if (!data) {
        data = new Widget(el);
        $element.data('widget.subscription', data);
      }
    });
  }
}

