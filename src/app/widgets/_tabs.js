
export default class Widget {

  constructor(element) {
    this.$root = $(element);
    this.locals = this._getDom();

    const target = this.getHash() || this.locals.$links.eq(1).attr('href');

    this._assignEvents();
    setTimeout(() => {
      this._activateTab(target);
    }, 1000);
  }

  getHash() {
    const hash = window.location.hash;

    return (hash === '#annual-tab' || hash === '#monthly-tab') ? hash : '';
  }

  _getDom() {
    const $root = this.$root;

    return {
      $links: $root.find('[data-tab-target]'),
      $tabs: $root.find('[data-tab-content]')
    };
  }

  _assignEvents() {
    this.$root
      .on('click', '[data-tab-target]', this._onClickTarget.bind(this));
  }

  _onClickTarget(e) {
    e.preventDefault();
    const target = $(e.currentTarget).attr('href');

    this._activateTab(target);
  }

  _activateTab(target) {
    const locals = this.locals;
    const $target = $(target);
    const $link = locals.$links.filter(`[href="${target}"]`);

    if (!$target.length || $target.hasClass('state-active')) return;

    locals.$links.removeClass('state-active');
    locals.$tabs.hide();

    $target.show();
    $link.addClass('state-active');
  }

  // static
  static plugin(selector) {
    const $elems = $(selector);
    if (!$elems.length) return;

    return $elems.each(function(index, el) {
      const $element = $(el);
      let data     = $element.data('widget.tabs');

      if (!data) {
        data = new Widget(el);
        $element.data('widget.tabs', data);
      }
    });
  }
}
