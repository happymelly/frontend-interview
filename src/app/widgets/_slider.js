export default class Widget {
  constructor(elem) {
    this.$root = $(elem);
    this.locals = this._getDom();

    this._init();
    this.setActiveDescription(0);
    this._assignEvents();
  }

  _init() {
    this.locals.$slider.slick({
      dots: false,
      infinite: true,
      centerMode: true,
      slidesToShow: 3,
      variableWidth: true
    });
  }

  _getDom() {
    const $root = this.$root;

    return {
      $slider: $root.find('[data-slider]'),
      $descriptions: $root.find('[data-slider-descr]')
    };
  }

  _assignEvents() {
    this.locals.$slider
      .on('afterChange', this._onChangeSlide.bind(this));
  }

  _onChangeSlide(event, slick, currentSlide, nextSlide) {
    this.setActiveDescription(currentSlide);
  }

  setActiveDescription(index) {
    this.locals.$descriptions
      .removeClass('state_active')
      .eq(index)
      .addClass('state_active');
  }

  static plugin(selector) {
    const $elems = $(selector);
    if (!$elems.length) return;

    return $elems.each(function(index, el) {
      const $element = $(el);
      let data = $element.data('widget.slider');

      if (!data) {
        data = new Widget(el);
        $element.data('widget.slider', data);
      }
    });
  }
}
