

export default class Widget {
  constructor(elem) {
    this.$root = $(elem);
    this.locals = this._getDom();
    this.isVisible = false;

    this._assignEvents();
  }

  _getDom() {
    const $root = this.$root;

    return {
      $body: $('body'),
      $popup: $root.find('[data-video-popup]'),
      $container: $root.find('[data-video-con]'),
      $template: $root.find('#video-iframe')
    };
  }

  _assignEvents() {
    this.$root
      .on('click', '[data-video-show]', this._onClickShow.bind(this))
      .on('click', '[data-video-close]', this._onClickClose.bind(this));
  }

  _onClickShow(e) {
    e.preventDefault();
    if (this.isVisible) return;

    const locals = this.locals;

    locals.$container.html(locals.$template.html());
    locals.$popup.addClass('state_active');
    locals.$body.css('overflow', 'hidden');
    this.isVisible = true;

  }

  _onClickClose(e) {
    e.preventDefault();
    if (!this.isVisible) return;

    const locals = this.locals;

    locals.$container.html('');
    locals.$popup.removeClass('state_active');
    locals.$body.css('overflow', 'auto');
    this.isVisible = false;
  }


  static plugin(selector) {
    const $elems = $(selector);
    if (!$elems.length) return;

    return $elems.each(function(index, el) {
      const $element = $(el);
      let data = $element.data('widget.popup');

      if (!data) {
        data = new Widget(el);
        $element.data('widget.popup', data);
      }
    });
  }
}
