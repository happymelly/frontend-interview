

export default class Widget {
  constructor(elem) {
    this.$root = $(elem);
    this.locals = this._getDom();

    this._assignEvents();
  }

  _getDom() {
    const $root = this.$root;

    return {
      $container: $root.find('[data-video-con]'),
      $template: $root.find('[data-video-temp]')
    };
  }

  _assignEvents() {
    this.$root
      .on('click', '[data-video-btn]', this._onClickShow.bind(this));
  }

  _onClickShow(e) {
    e.preventDefault();
    const locals = this.locals;
    const template = locals.$template.html();

    this.$root.addClass('n-video_state_show');
    locals.$container.html(template);
  }


  static plugin(selector) {
    const $elems = $(selector);
    if (!$elems.length) return;

    return $elems.each(function(index, el) {
      const $element = $(el);
      let data = $element.data('widget.video');

      if (!data) {
        data = new Widget(el);
        $element.data('widget.video', data);
      }
    });
  }
}
