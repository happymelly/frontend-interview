

export default class Widget {
  constructor(elem) {
    const self = this;
    this.$root = $(elem);
    this.locals = this._getDom();

    this.getData()
      .done((resp) => {
        const data = $.parseJSON(resp);
        self._renderData(data);
      });
  }

  _getDom() {
    const $root = this.$root;

    return {
      $attendees: $root.find('[data-stat-attend]'),
      $trainers: $root.find('[data-stat-trainers]'),
      $evaluations: $root.find('[data-stat-eval]'),
      $brands: $root.find('[data-stat-brand]')
    };
  }

  _renderData(data) {
    const locals = this.locals;

    locals.$attendees.text(data.attendees);
    locals.$trainers.text(data.trainers);
    locals.$evaluations.text(data.evaluations);
    locals.$brands.text(data.providers);
  }

  getData() {
    return $.get('https://workshopbutler.com/public-statistics');
  }

  static plugin(selector) {
    const $elems = $(selector);
    if (!$elems.length) return;

    return $elems.each(function(index, el) {
      const $element = $(el);
      let data = $element.data('widget.stat');

      if (!data) {
        data = new Widget(el);
        $element.data('widget.stat', data);
      }
    });
  }
}

