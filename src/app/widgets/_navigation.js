export default class Widget {
  constructor(elem) {
    this.$root = $(elem);
    this.locals = this._getDom();
    this.params = this._getParams();

    this._assignEvents();
    setTimeout(() => {
      this.updatePostiion();
    }, 1300);
  }

  _getDom() {
    const $root = this.$root;

    return {
      $helper: $root.find('[data-nav-helper]'),
      $menu: $root.find('[data-nav-menu]')
    };
  }

  _getParams() {
    const locals = this.locals;
    return {
      menuOffsetTop: locals.$menu.offset().top
    };
  }

  _assignEvents() {
    this.$root
      .on('click', '[data-nav-link]', this._onClickGoTo.bind(this));

    $(window)
      .scroll(this.updatePostiion.bind(this))
      .resize(this._onResize.bind(this));
  }

  _onResize() {
    this.updateParams.bind(this);
    this.updatePostiion.bind(this);
  }

  updatePostiion(e) {
    const locals = this.locals;
    const isSetFixed = $(window).scrollTop() >= this.params.menuOffsetTop;

    if (isSetFixed) {
      if (locals.$menu.hasClass('state_fixed')) return;

      locals.$menu.addClass('state_fixed');
      locals.$helper.show();
    } else {
      if (!locals.$menu.hasClass('state_fixed')) return;

      locals.$menu.removeClass('state_fixed');
      locals.$helper.hide();
    }
  }

  updateParams() {
    this.params.menuOffsetTop = this.locals.$menu.offset().top;
  }

  _onClickGoTo(e) {
    e.preventDefault();
    const target = $(e.currentTarget).attr('href');
    const $target = $(target);

    if (!$target.length) return;

    $('html, body').animate({
      scrollTop: $target.offset().top - 100
    }, 400);
  }

  static plugin(selector) {
    const $elems = $(selector);
    if (!$elems.length) return;

    return $elems.each(function(index, el) {
      const $element = $(el);
      let data = $element.data('widget.navigation');

      if (!data) {
        data = new Widget(el);
        $element.data('widget.navigation', data);
      }
    });
  }
}

