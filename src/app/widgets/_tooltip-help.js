

export default class Widget {
  /**
     * Filter history
     * @param {String} selector
     */
  constructor(selector) {
    this.$link = $(selector);
    this.$body = $('body');

    this._init();
    this._assignEvents();
  }

  _init() {
    const template = this._getTemplate();

    this.isVisible = false;
    this.$tooltip = $(template).appendTo('body');
    this.$tooltip.plugin = this;
  }

  _getTemplate() {
    const $link = this.$link;
    const tempData = {
      title: $link.data('help-title') || '',
      content: $link.data('help-content')
    };

    return `
            <div class="b-toolhelp">
                <a href="#" class="b-toolhelp__close" data-help-close></a>
                <div class="b-toolhelp__content">
                    <div class="title">${tempData.title}</div>
                    ${tempData.content}
                </div>
            </div>
        `;
  }

  _setPosition() {
    const $link = this.$link;
    const $tooltip = this.$tooltip;

    $tooltip.addClass('b-toolhelp_state_right');
    $tooltip.css({
      left: $link.offset().left + $link.width(),
      top: $link.offset().top + $link.height() / 2
    });
  }

  _assignEvents() {
    this.$link
      .on('mouseenter', this._onMouseEnter.bind(this));
    this.$body
      .on('click', this._onClickGlobal.bind(this))
      .on('click', '[data-help-close]', this._onClickClose.bind(this));

    App.events
      .sub('hmt.help.close', this._onEventClose.bind(this));
  }

  _onEventClose(data) {
    if (data.$tooltip === this.$tooltip) return;
    this.closeTooltip(this);
  }

  _onMouseEnter() {
    this.showTooltip();
  }

  _onClickClose(e) {
    e.preventDefault();
    this.closeTooltip();
  }

  _onClickGlobal(e) {
    const $parent = $(e.target).closest('.b-toolhelp');

    if ($parent.length) return;
    this.closeTooltip();
  }

  showTooltip() {
    this._setPosition();

    App.events.pub('hmt.help.close', {
      $tooltip: this.$tooltip
    });

    if (this.isVisible) return;

    this.isVisible = true;
    this.$tooltip.addClass('b-toolhelp_show');
  }

  closeTooltip() {
    if (!this.isVisible) return;

    this.isVisible = false;
    this.$tooltip.removeClass('b-toolhelp_show');
  }

  // static
  static plugin(selector) {
    const $elems = $(selector);
    if (!$elems.length) return;

    return $elems.each(function(index, el) {
      const $element = $(el);
      let data     = $element.data('widget.tooltip.help');

      if (!data) {
        data = new Widget(el);
        $element.data('widget.tooltip.help', data);
      }
    });
  }
}
