

export default class Widget {
  constructor(elem) {
    this.$root = $(elem);
    this.locals = this._getDom();

    this._assignEvents();
  }

  _getDom() {
    return {
      $nav: $('.b-nav')
    };
  }

  _assignEvents() {
    this.$root
      .on('click', '[data-search-close]', this._onClickClose.bind(this))
      .on('focus', 'input[type="search"]', this._onFocus.bind(this));
  }

  _onClickClose(e) {
    e.preventDefault();
    this.locals.$nav.removeClass('b-nav_show_search');
  }

  _onFocus() {
    if (this.locals.$nav.hasClass('b-nav_show_search')) return;
    this.locals.$nav.addClass('b-nav_show_search');
  }


  static plugin(selector) {
    const $elems = $(selector);
    if (!$elems.length) return;

    return $elems.each(function(index, el) {
      const $element = $(el);
      let data = $element.data('widget.search');

      if (!data) {
        data = new Widget(el);
        $element.data('widget.search', data);
      }
    });
  }
}

