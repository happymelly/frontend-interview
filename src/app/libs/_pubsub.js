const pubsub = {
  subscribers: {},
  alreadyPublished: {},

  /**
   *
   * @param types
   * @param fn
   * @param context
   * @param once
   * @return {*}
   */
  sub: function(events, fn, context, once, runIfAlreadyPublished) {
    (this.sanitizeEvents(events)).forEach(function(event) {

      var subscriberObject = {
        fn: fn,
        context: context,
        once: once
      };

      if (runIfAlreadyPublished && this.alreadyPublished[event]) {
        this.executeSubscriber(subscriberObject, null);

        if (once) {
          return;
        }
      }

      (this.subscribers[event] = this.subscribers[event] || []).push(subscriberObject);
    }, this);

    return this;
  },

  /**
   * Add subscriber on events
   *
   * @param types
   * @param fn
   * @param context
   * @return this App.events
   */
  subOnce: function(types, fn, context) {
    return this.sub(types, fn, context, true);
  },

  /**
   * Delete subscribe
   *
   * @param types
   * @param fn callback
   * @return this App.events
   */
  unSub: function(types, fn, context) {
    this.sanitizeEvents(types).forEach(function(type) {
      (this.subscribers[type] = this.subscribers[type] || []).forEach(function(subscriber, index) {
        if (subscriber.fn === fn || subscriber.context && subscriber.context === context) {
          this.subscribers[type].splice(index, 1);
          return false;
        }
      }, this);
    }, this);

    return this;
  },

  /**
   * Delete all subscribers
   *
   * @param types
   * @return this App.events
   */
  unSubAll: function(types) {
    this.sanitizeEvents(types).forEach(function(type) {
      (this.subscribers[type] = this.subscribers[type] || []).forEach(function(subscriber, index) {
        this.subscribers[type] = [];
        return false;
      }, this);
    }, this);

    return this;
  },

  /**
   * Run event. The first parameter - events, other - parameters for  unction
   *
   * @param events СЃРѕР±С‹С‚РёСЏ С‡РµСЂРµР· РїСЂРѕР±РµР»
   * @return {*}
   */
  pub: function(events/*, arg1, arg2, arg3*/) {
    var argsArr = this.argsToArray(arguments).slice(1);

    this.sanitizeEvents(events).forEach(function(event) {
      this.subscribers[event] = this.subscribers[event] || [];
      this.alreadyPublished[event] = true;

      for (var index = 0, size = this.subscribers[event].length; index < size; index++) {
        var subscriber = this.subscribers[event][index];

        this.executeSubscriber(subscriber, argsArr);

        if (subscriber.once) {
          this.subscribers[event].splice(index, 1);
          index--;
          size--;
        }
      }

    }, this);

    return this;
  },

  executeSubscriber: function(subscriber, argsArr) {
    subscriber.fn.apply(subscriber.context, argsArr);
  },


  sanitizeEvents: function(events) {
    events = events.split(' ');
    if (events.length === 0) {
      events = ['*'];
    }
    return events;
  },

  argsToArray: function(args) {
    var arr = [];
    for (var i = 0; typeof args[i] != 'undefined'; i++) {
      arr.push(args[i]);
    }
    return arr;
  }
};

export default pubsub;
